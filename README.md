# Постановка задачи

У преподавателя может быть несколько студентов, у студентов может
быть несколько преподавателей.
Т.е. это связь многие ко многим, которая реализуется через сводную
таблицу. Таблица <B>links</B> является такой таблицей она
содержит связи между учителями и учениками. Каждая связь имеет
уникальный композитный ключ который состоит из id преподавателя и id студента.

Для построения и заполнения базы данных используется
<a href="../../tree/develop/src/main/java/com/example/demo/learning_database.sql">SQL файл</a>,
который содержит в себе команды для создания таблиц, построения
связей между ними и заполнения таблиц. База данных имеет название <B>learning</B>

База данных имеет следующую структуру:

<a href="../../tree/develop/src/main/java/com/example/demo/learning_database.sql"><img src="/.img/uml_scheme.png" width="500" /></a>

Стоит отметить, что для того, чтобы удалить ученика или учителя,
нужно удалить все связи которые они имеют в таблице links.

Чтобы БД подключилась к проекту, нужно внести в application.properties название 
созданной по SQL БД, username и password. 

        spring.datasource.url=jdbc:postgresql://localhost:5432/learning
        spring.datasource.username=postgres
        spring.datasource.password=12345
        spring.jpa.generate-ddl=true
        
        spring.datasource.driver-class-name=org.postgresql.Driver
        spring.jpa.database=postgresql
        spring.jpa.database-platform=org.hibernate.dialect.PostgreSQL10Dialect

# Краткое описание API

    Доступно по адресу http://localhost:8080

><img src="/.img/api.png" width="700" />

# Обзор возможностей

    Custom White page

><img src="/.img/whitepage.png" width="500" />

    Список студентов

><img src="/.img/studlist.png" width="500" />

    Экспорт JSON

><img src="/.img/json.png" width="500" />

    Экспорт XML

><img src="/.img/xml.png" width="500" />

    Таблица связей

><img src="/.img/linktable.png" width="700" />

    Список студентов прикрепленных к преподавателю

><img src="/.img/listsoft.png" width="500" />

# Тестирование ответов с помощью RESTED

><img src="/.img/rested.png" width="700" />