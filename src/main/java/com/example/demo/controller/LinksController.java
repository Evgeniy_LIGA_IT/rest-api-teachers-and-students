package com.example.demo.controller;

import com.example.demo.domain.*;
import com.example.demo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class LinksController {

    @Autowired
    private StudentsRepository studentsRepo;
    @Autowired
    private TeachersRepository teachersRepo;
    @Autowired
    private LinksRepository linksRepo;

    private final String path = "/link";
    private final String paths = "/links";

    //region Обработка запроса №13. На отображение списка всех связей

    public Iterable<Links> getAllLinks() { return linksRepo.findAll(); }

    // Простая обертка для получения данных в JSON формате
    @ResponseBody @RequestMapping(value = {paths,paths+".json"}, method = GET, produces={MediaType.APPLICATION_JSON_VALUE})
    public Iterable<Links> returnLinksJSON() { return getAllLinks(); }

    // Простая обертка для получения данных в XML формате
    @ResponseBody @RequestMapping(value = paths+".xml", method = GET, produces = MediaType.APPLICATION_XML_VALUE)
    public Iterable<Links> returnLinksXML() { return getAllLinks(); }

    // Обертка для получения данных в виде HTML страницы с таблицей
    @RequestMapping(value = {paths+".htm",paths+".html"})
    public String returnLinksHTML(Model model) {
        Iterable<Links> links = getAllLinks();
        // По хорошему нужно создать какой-то левый объект для передачи данных
        List<String> comments = new ArrayList<String>();
        for (Links l : links ) {
            String com="";
            Integer t_id = l.getTeacher_id();
            if(teachersRepo.existsById(t_id)){
                Teachers t = teachersRepo.findById(t_id).get();
                com += "(Учитель) "+t.getName()+" "+t.getSurname();
            }
            com+=" <-> ";
            Integer s_id = l.getStudent_id();
            if(studentsRepo.existsById(s_id)){
                Students s = studentsRepo.findById(s_id).get();
                com += "(Студент) "+s.getName()+" "+s.getSurname();
            }
            comments.add(com);
        }
        model.addAttribute("comments_list", comments);
        model.addAttribute("list", links);
        return "links_table";
    }

    //endregion

    //region Обработка запроса №9 и №10. Добавить/удалить связь

    public String link(Integer tid, Integer sid, Boolean contype, Model model, HttpServletResponse response) {

        // Проверяем что преподаватель не null
        if(tid==null){
            // Если связь не нашли
            model.addAttribute("name", "Status 404. Невозможно создать связь. Id преподавателя равен null!");
            response.setStatus(404);
            return returnLinksHTML(model);
        }

        // Проверяем что студент не null
        if(sid==null){
            // Если связь не нашли
            model.addAttribute("name", "Status 404. Невозможно создать связь. Id студента равен null!");
            response.setStatus(404);
            return returnLinksHTML(model);
        }

        // Проверяем наличие такого преподавателя
        if(!teachersRepo.existsById(tid)){
            // Если связь не нашли
            model.addAttribute("name", "Status 404. Невозможно создать связь. Преподаватель с id "+tid+" не найден.");
            response.setStatus(404);
            return returnLinksHTML(model);
        }

        // Проверяем наличие такого студента
        if(!studentsRepo.existsById(sid)){
            model.addAttribute("name", "Status 404. Невозможно создать связь. Студент с id "+sid+" не найден.");
            response.setStatus(404);
            return returnLinksHTML(model);
        }

        CompositeId cid = new CompositeId(tid,sid);
        if(contype==true){
            // Если нужно добавить связь
            // Проверяем если искомая связь не существует, тогда пробуем создать ее
            if(!linksRepo.existsById(cid)){
                linksRepo.save(new Links(tid,sid));
                model.addAttribute("name", "Status 200. Связь добавлена.");
                response.setStatus(200);
            } else {
                // а если она есть значит не нужно ничего создавать.
                model.addAttribute("name", "Status 200. Данная связь уже существует.");
                response.setStatus(200);
            }
        } else {
            // Если нужно удалить связь
            // Проверяем если искомая связь существует, тогда удаляем ее
            if(linksRepo.existsById(cid)){
                linksRepo.deleteById(cid);
                model.addAttribute("name", "Status 200. Связь удалена.");
                response.setStatus(200);
            } else {
                // Если связь не нашли
                model.addAttribute("name", "Status 404. Искомая связь "+
                        " tid="+tid+" sid="+sid+" не найдена.");
                response.setStatus(404);
            }
        }
        // Так не делается конечно....
        return returnLinksHTML(model);
    }

    @RequestMapping(value = path+"/{link_type}",
            method = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
    public String addPeopleRequest(
            @PathVariable("link_type") String link_type,
            @RequestParam(required = false) Integer tid,
            @RequestParam(required = false) Integer sid,
            Model model, HttpServletResponse response) {
        if(link_type.equals("add")) {
            return link(tid, sid, true, model, response);
        } else if (link_type.equals("del")) {
            return link(tid, sid, false, model, response);
        }
        return "error";
    }

    // endregion

}
