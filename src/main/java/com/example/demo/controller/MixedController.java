package com.example.demo.controller;

import com.example.demo.domain.*;
import com.example.demo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class MixedController {

    @Autowired
    private StudentsRepository studentsRepo;
    @Autowired
    private TeachersRepository teachersRepo;
    @Autowired
    private LinksRepository linksRepo;

    private String crudToHTMLtemplate(CrudRepository crud){
        if(crud == teachersRepo)return "teachers_table";
        if(crud == studentsRepo)return "students_table";
        throw new RuntimeException("Unknown crud type");
    }

    //region Обработка запроса №1 и №5. Добавить студента/преподавателя

    public String addPeople(CrudRepository crud, String name, String surname, Model model, HttpServletResponse response) {
        // немного неправильно...
        People people = null;
        if(crud == teachersRepo) people = new Teachers(name,surname);
        if(crud == studentsRepo) people = new Students(name,surname);
        crud.save(people);
        model.addAttribute("name", "Status 200. Ok. "+people.Typename()+ " '" + people.getName() +
        " " + people.getSurname() + "' добавлен");
        response.setStatus(200);
        Iterable<People> list = (Iterable<People>) crud.findAll();
        model.addAttribute("list", list);
        return crudToHTMLtemplate(crud);
    }

    @RequestMapping(value = "/{who}/add",
            method = {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT})
    public String addPeopleRequest(
            @PathVariable("who") String who, @RequestParam String name,
            @RequestParam String surname, Model model, HttpServletResponse response) {
        if(who.equals("student")) {
            return addPeople(studentsRepo, name, surname, model, response);
        } else if (who.equals("teacher")) {
            return addPeople(teachersRepo, name, surname, model, response);
        }
        return "error";
    }

    // endregion

    //region Обработка запроса №2 и №6. Удалить студента/преподавателя

    public String delPeople(CrudRepository crud, Integer id, Model model, HttpServletResponse response) {
        if(id==null) {
            response.setStatus(404);
            model.addAttribute("name", "Status 404. Не найдено. Вы указали Id == null");
        } else {
            if(crud.existsById(id)) {
                try {
                    People s = (People) crud.findById(id).orElse(null);
                    crud.deleteById(id);
                    model.addAttribute("name", "Status 200. Ok. "+s.Typename()+" c Id=" + id + " '" + s.getName() +
                            " " + s.getSurname() + "' удален");
                    response.setStatus(200);
                } catch (RuntimeException e){
                    response.setStatus(424);
                    model.addAttribute("name", "Status 424. Ошибка SQL. Удалите все связи сущности, прежде чем её удалять! ");
                    System.out.println("Error: "+e);
                }
            } else {
                response.setStatus(404);
                model.addAttribute("name", "Status 404. Не найдено. Запись с Id="+id+" отсутствует в списке");
            }
        }
        Iterable<People> p = (Iterable<People>) crud.findAll();
        model.addAttribute("list", p);
        return crudToHTMLtemplate(crud);
    }

    @RequestMapping(value = "/{who}/del",
            method = {RequestMethod.GET,RequestMethod.POST, RequestMethod.DELETE})
    public String delPeopleRequest(@PathVariable("who") String who, @RequestParam Integer id, Model model, HttpServletResponse response) {
        if(who.equals("student")) {
            return delPeople(studentsRepo, id, model, response);
        } else if (who.equals("teacher")) {
            return delPeople(teachersRepo, id, model, response);
        }
        return "error";
    }

    // endregion

}
