package com.example.demo.controller;

import com.example.demo.domain.*;
import com.example.demo.repository.*;
import com.example.demo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class TeacherController {

    @Autowired
    private PeopleServices people_services;

    @Autowired
    private TeachersRepository repo;
    @Autowired
    private StudentsRepository opposite_repo;
    private String name = "Преподаватель";
    private final String name_of = "преподавателя";
    private final String path = "/teacher";
    private final String people_links_table_HTML = "teacher_links_table";
    private final String names_of = "преподователей";
    private final String path_all = "/teachers";
    private final String people_table_HTML = "teachers_table";

    //region Обработка запроса (№3 или №7). Отображение одной сущности

    // Получение данных в JSON формате
    @RequestMapping(value = {path+"/get",path+"/get.json"},
            method = {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT},
            produces={MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Object getOnePeopleJSON(@RequestParam(required = false) Integer id, HttpServletResponse response) {
        return people_services.getPeople(id, response, repo, name_of);
    }

    // Получение данных в XML формате
    @RequestMapping(value = path+"/get.xml",
            method = {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT},
            produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public Object getOnePeopleXML(@RequestParam(required = false) Integer id, HttpServletResponse response) {
        return people_services.getPeople(id, response, repo, name_of);
    }

    //endregion

    //region Обработка запроса (№11 или №12). Отображение списка сущностей приписанных к одной сущности.

    // Получение данных в JSON формате
    @RequestMapping(value = {path+"/links",path+"/links.json"},
            method = {RequestMethod.GET,RequestMethod.POST},
            produces={MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public List<People> getPeopleLinksJSON(@RequestParam Integer id, HttpServletResponse response) {
        return people_services.getPeopleLinks(id, response, repo, opposite_repo);
    }

    // Получение данных в XML формате
    @RequestMapping(value = path+"/links.xml",
            method = {RequestMethod.GET,RequestMethod.POST},
            produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public List<People> getPeopleLinksXML(@RequestParam Integer id, HttpServletResponse response) {
        return people_services.getPeopleLinks(id, response, repo, opposite_repo);
    }

    @RequestMapping(value = {path+"/links.htm",path+"/links.html"},
            method = {RequestMethod.GET,RequestMethod.POST})
    public String getPeopleLinksHTML(
            @RequestParam Integer id,
            @RequestParam(required = false, name="action") String action,
            @RequestParam(required = false, name="oid") Integer oid,
            Model model, HttpServletResponse response) {

        // Обрабатываем запрос add/del link если он встроен в запрос.
        boolean im_student = ((CrudRepository)repo instanceof StudentsRepository);
        people_services.HandleAddDel(action,id,oid,im_student);
        // Получаем список сущностей
        List<People> list = people_services.getPeopleLinks(id, response, repo, opposite_repo);
        // Заполняем HTML шаблон
        people_services.getPeopleLinksFillHTMLTemplate(list, name_of, id, model, response);
        // Выводим ответ
        return people_links_table_HTML;
    }

    //endregion

    //region Обработка запроса (№4 или №8). Отображение списка всех сущностей

    // Получение данных в JSON формате
    @RequestMapping(value = {path_all,path_all+".json"},
            method = GET, produces={MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Iterable<People> returnAllPeoplesJSON() {
        return people_services.getAll(repo);
    }

    // Получение данных в XML формате
    @RequestMapping(value = path_all+".xml",
            method = GET, produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public Iterable<People> returnAllPeoplesXML() {
        return people_services.getAll(repo);
    }

    // Получение данных в HTML формате
    @RequestMapping(value = {path_all+".htm",path_all+".html"})
    public String returnAllPeoplesHTML(Model model) {
        model.addAttribute("list", people_services.getAll(repo));
        model.addAttribute("name", "Список "+names_of);
        return people_table_HTML;
    }

    //endregion

}
