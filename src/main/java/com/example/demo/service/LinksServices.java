package com.example.demo.service;

import com.example.demo.domain.CompositeId;
import com.example.demo.domain.Links;
import com.example.demo.domain.Students;
import com.example.demo.repository.LinksRepository;
import com.example.demo.repository.StudentsRepository;
import com.example.demo.repository.TeachersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LinksServices {

    @Autowired
    private StudentsRepository studentsRepo;
    @Autowired
    private TeachersRepository teachersRepo;
    @Autowired
    private LinksRepository linksRepo;

    public String Link(Integer tid, Integer sid, Boolean contype) {

        // Проверяем что преподаватель не null
        if(tid==null){
            return "Status 404. Невозможно создать связь. Id преподавателя равен null!";
        }

        // Проверяем что студент не null
        if(sid==null){
            return "Status 404. Невозможно создать связь. Id студента равен null!";
        }

        // Проверяем наличие такого преподавателя
        if(!teachersRepo.existsById(tid)){
            return "Status 404. Невозможно создать связь. Преподаватель с id "+tid+" не найден.";
        }

        // Проверяем наличие такого студента
        if(!studentsRepo.existsById(sid)){
            return "Status 404. Невозможно создать связь. Студент с id "+sid+" не найден.";
        }

        CompositeId cid = new CompositeId(tid,sid);
        if(contype==true){
            // Если нужно добавить связь
            // Проверяем если искомая связь не существует, тогда пробуем создать ее
            if(!linksRepo.existsById(cid)){
                linksRepo.save(new Links(tid,sid));
                return "Status 200. Связь добавлена.";
            } else {
                // а если она есть значит не нужно ничего создавать.
                return "Status 200. Данная связь уже существует.";
            }
        } else {
            // Если нужно удалить связь
            // Проверяем если искомая связь существует, тогда удаляем ее
            if(linksRepo.existsById(cid)){
                linksRepo.deleteById(cid);
                return "Status 200. Связь удалена.";
            } else {
                // Если связь не нашли
                return "Status 404. Искомая связь "+
                        " tid="+tid+" sid="+sid+" не найдена.";
            }
        }
    }

}
