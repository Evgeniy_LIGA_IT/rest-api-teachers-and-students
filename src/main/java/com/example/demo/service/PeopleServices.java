package com.example.demo.service;

import com.example.demo.domain.CompositeId;
import com.example.demo.domain.Error;
import com.example.demo.domain.Links;
import com.example.demo.domain.People;
import com.example.demo.repository.LinksRepository;
import com.example.demo.repository.StudentsRepository;
import com.example.demo.repository.TeachersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Service
public class PeopleServices {

    @Autowired
    private LinksRepository links_repo;
    @Autowired
    private LinksServices links_services;

    public Iterable<People> getAll(CrudRepository repo) {
        return repo.findAll();
    }

    public Object getPeople(Integer id, HttpServletResponse response, CrudRepository repo, String name_of) {
        // Проверка id на null
        if(id == null){
            response.setStatus(404);
            return new Error("Ошибка: Id "+name_of+" == null.");
        }
        // Проверка id на наличие
        if(!repo.existsById(id)){
            response.setStatus(404);
            return new Error("Ошибка: Id "+name_of+" не найден.");
        }
        // в остальных случаях все ок
        response.setStatus(200);
        return repo.findById(id).orElse(null);
    }

    public List<People> getPeopleLinks(Integer id, HttpServletResponse response, CrudRepository repo, CrudRepository opposite_repo) {
        List<People> list = new ArrayList<People>();
        // если null или такого нет
        if( id == null || !repo.existsById(id)){
            response.setStatus(404);
            return null;
        }

        Iterable<Links> links = links_repo.findAll();
        //System.out.print((repo instanceof StudentsRepository)+"? ");
        //System.out.println((opposite_repo instanceof StudentsRepository)+"?");
        for (Links l : links ) {
            // Чтобы понять как правильно работать с ссылкой нужно знать кто из repo
            // и opposite_repo является StudentsRepo, а кто TeachersRepo.
            // для этого используем instanceof.
            int opp_link = l.getStudent_id();
            int link = l.getTeacher_id();
            if(repo instanceof StudentsRepository) {
                link = l.getStudent_id();
                opp_link = l.getTeacher_id();
            }
            if(link==id){
                if(opposite_repo.existsById(opp_link)){
                    list.add((People)opposite_repo.findById(opp_link).get());
                }
            }
        }
        response.setStatus(200);
        return list;
    }

    public void getPeopleLinksFillHTMLTemplate(List<People> list, String name_of, Integer id, Model model, HttpServletResponse response) {
        if(response.getStatus() == 404)
            model.addAttribute("name", "Status 404. Id == null, либо не найден.");
        if(response.getStatus() == 200)
            model.addAttribute("name", "Status 200. Список сущностей "+name_of+" с id="+id);
        model.addAttribute("id", id);
        model.addAttribute("list", list);
        return;
    }

    public String HandleAddDel(String action, Integer id, Integer oid, boolean im_student){
        if(action!=null) {
            // У противоположного класса инверсия
            Integer tid=id;
            Integer sid=oid;
            if(im_student){
                sid=id;
                tid=oid;
            }
            if(action.equals("add"))
                return links_services.Link(tid,sid,true);
            if(action.equals("del"))
                return links_services.Link(tid,sid,false);
        }
        return "";
    }

}
