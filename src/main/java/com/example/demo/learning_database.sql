-- База данных learning
CREATE TABLE teachers (
                          teacher_id      integer,
                          name        varchar(50),
                          surname     varchar(50)
);
CREATE TABLE students (
                          student_id      integer,
                          name        varchar(50),
                          surname     varchar(50)
);
CREATE TABLE links (
                       teacher_id      integer NOT NULL,
                       student_id      integer NOT NULL,
                       PRIMARY KEY (teacher_id,student_id)
);
-- Т.к primary key по умолчанию unique такие же
-- constraint мы должны назначить одноименным
-- полям в таблицах teachers и students
ALTER TABLE teachers
    ADD CONSTRAINT teachers_unique UNIQUE (teacher_id);
ALTER TABLE students
    ADD CONSTRAINT students_unique UNIQUE (student_id);
-- Только после этого можно установить связи связи
ALTER TABLE links
    ADD FOREIGN KEY (teacher_id)
        REFERENCES teachers(teacher_id);
ALTER TABLE links
    ADD FOREIGN KEY (student_id)
        REFERENCES students(student_id);
-- Заполним таблицу некоторыми данными
INSERT INTO teachers VALUES
(1,'Евгения','Семина'),
(2,'Зоя','Котова'),
(3,'Иван','Петров'),
(4,'Валерия','Ефимова'),
(5,'Мария','Смирнова');
INSERT INTO students VALUES
(1,'Елисей','Павленко'),
(2,'Петр','Петров'),
(3,'Иван','Николаев'),
(4,'Сергей','Семин'),
(5,'Валерий','Старков'),
(6,'Алексей','Семин'),
(7,'Михаил','Степанов');
-- устанавливаем связи
INSERT INTO links VALUES
(1,2),
(1,5),
(1,7),
(2,1),
(2,4),
(3,6),
(4,1),
(4,4),
(4,5),
(5,3),
(5,6);