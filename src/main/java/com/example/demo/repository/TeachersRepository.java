package com.example.demo.repository;

import com.example.demo.domain.Students;
import com.example.demo.domain.Teachers;
import org.springframework.data.repository.CrudRepository;

public interface TeachersRepository extends CrudRepository<Teachers, Integer> {

    //List<Students> findAllById(Integer student_id);//просто правильное название метода даст возможность
    //избежать запросов на SQL

    //@Query("select * from jobs") //если этого мало можно написать
        //собственный запрос на языке похожем на SQL
    //List<Jobs> findAllQ();

    //@Query(value = "select * from users where name like '%smith%'", nativeQuery = true)
        //если и этого мало - можно написать запрос на чистом SQL и все это будет работать
    //List<Jobs> findWhereNameStartsFromSmith();

}
