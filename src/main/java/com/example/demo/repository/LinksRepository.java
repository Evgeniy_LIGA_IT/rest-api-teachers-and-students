package com.example.demo.repository;

import com.example.demo.domain.CompositeId;
import com.example.demo.domain.Links;
import com.example.demo.domain.Students;
import org.springframework.data.repository.CrudRepository;

// Поле @Id в dto должно быть того же типа как и -> CrudRepository<Students, ???>
public interface LinksRepository extends CrudRepository<Links, CompositeId> {

    //List<Students> findAllById(Integer student_id);//просто правильное название метода даст возможность
    //избежать запросов на SQL

    //@Query("select * from jobs") //если этого мало можно написать
        //собственный запрос на языке похожем на SQL
    //List<Jobs> findAllQ();

    //@Query(value = "select * from users where name like '%smith%'", nativeQuery = true)
        //если и этого мало - можно написать запрос на чистом SQL и все это будет работать
    //List<Jobs> findWhereNameStartsFromSmith();

}
