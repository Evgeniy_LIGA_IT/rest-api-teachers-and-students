package com.example.demo.domain;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "teachers")
@XmlRootElement(name = "Root")
public class Teachers implements People {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int teacher_id;

    private String name;
    private String surname;

    public Teachers() {
    }

    public Teachers(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    @XmlElement
    public int getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(int teacher_id) {
        this.teacher_id = teacher_id;
    }

    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Transient
    public String Typename() {
        return "Преподаватель";
    }
}
