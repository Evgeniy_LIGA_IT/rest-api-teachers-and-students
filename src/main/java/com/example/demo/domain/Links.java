package com.example.demo.domain;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


@Entity
@XmlRootElement(name = "Root")
@Table(name = "links")
@IdClass(CompositeId.class)
public class Links implements Serializable {

    @Id
    private Integer teacher_id;

    @Id
    private Integer student_id;

    public Links() {
    }

    public Links(Integer teacher_id, Integer student_id) {
        this.teacher_id = teacher_id;
        this.student_id = student_id;
    }

    public Integer getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(Integer teacher_id) {
        this.teacher_id = teacher_id;
    }

    public Integer getStudent_id() {
        return student_id;
    }

    public void setStudent_id(Integer student_id) {
        this.student_id = student_id;
    }
}
