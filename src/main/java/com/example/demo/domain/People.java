package com.example.demo.domain;

import javax.persistence.Transient;

public interface People {

    public String getName();
    public String getSurname();

    // Ни в коем случае не называть метод с get иначе его содержимое
    // будет вставлено в ответ на запрос несмотря на @Transient
    @Transient
    public String Typename();

}
