package com.example.demo.domain;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name = "Root")
public class Students implements People {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer student_id;

    private String name;
    private String surname;

    public Students() {
    }

    public Students(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    @XmlElement
    public Integer getStudent_id() {
        return student_id;
    }

    public void setStudent_id(Integer student_id) {
        this.student_id = student_id;
    }

    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Transient
    public String Typename() {
        return "Студент";
    }
}
