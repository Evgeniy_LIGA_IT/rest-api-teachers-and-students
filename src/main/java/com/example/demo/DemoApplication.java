package com.example.demo;

import com.example.demo.domain.Links;
import com.example.demo.repository.LinksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class DemoApplication {

    // Без Autowired работать не будет
    @Autowired
    private LinksRepository linksRepo;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    private void testJpaMethods() {
        System.out.println("Links:");
        Iterable<Links> links = linksRepo.findAll();
        if(links!=null)
            links.forEach(it-> System.out.println(
                "s="+it.getStudent_id()+" t="+ it.getTeacher_id()));
    }

}
